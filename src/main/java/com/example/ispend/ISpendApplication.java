package com.example.ispend;

import com.example.ispend.entity.BudgetItemEntity;
import com.example.ispend.repository.BudgetRepository;
import com.example.ispend.service.BudgetService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

@SpringBootApplication
public class ISpendApplication {

    public static void main(String[] args) {

        SpringApplication.run(ISpendApplication.class, args);

    }

}
