package com.example.ispend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name="category")
public class CategoryEntity {


    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name="created_by_user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @Column(name="category")
    private String category;

    @Column(name="parent_category", nullable = true)
    private long parentCategoryId;

    @Column(name="description")
    private String description;

    @Column(name="created_at")
    private Instant createdAt;

}
