package com.example.ispend.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name="income")
public class IncomeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name="created_by_user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @Column(name="income")
    private BigDecimal income;

    @Column(name = "currency")
    private String currency;

    @Column(name = "created_at")
    private Instant createdAt;


}
