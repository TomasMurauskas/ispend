package com.example.ispend.entity;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name="budget")
public class BudgetItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name="created_by_user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @Column(name="budget_item")
    private String budgetItem;

    @ManyToOne(optional = false)
    @JoinColumn(name="category_id", referencedColumnName = "id")
    private CategoryEntity categoryEntity;

    @Column(name="amount")
    private BigDecimal amount;

    @Column(name="currency")
    private String currency;

    @Column(name="start_date")
    private LocalDate startDate;

    @Column(name="end_date")
    private LocalDate endDate;

    @Column(name="created_at")
    private Instant createdAt;
}
