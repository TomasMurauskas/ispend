package com.example.ispend.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "transaction_table")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long Id;

    @ManyToOne(optional = false)
    @JoinColumn(name="created_by_user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @Column(name="transaction_item")
    private String transactionItem;

    @ManyToOne(optional = false)
    @JoinColumn(name="category_id", referencedColumnName = "id")
    private CategoryEntity categoryEntity;

    @ManyToOne(optional = false)
    @JoinColumn(name="budget_item_id", referencedColumnName = "id")
    private BudgetItemEntity budgetItemEntity;

    @Column(name="amount")
    private BigDecimal amount;

    @Column(name="currency")
    private String currency;

    @Column(name="transaction_date")
    private LocalDate transactionDate;

    @Column(name="description")
    private String description;

    @Column(name="created_at")
    private Instant createdAt;


}
