package com.example.ispend.money.classes;

import javax.money.*;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormatter implements MonetaryAmount{


    @Override
    public MonetaryContext getContext() {
        return null;
    }

    @Override
    public MonetaryAmountFactory<? extends MonetaryAmount> getFactory() {
        return null;
    }

    @Override
    public boolean isGreaterThan(MonetaryAmount monetaryAmount) {
        return false;
    }

    @Override
    public boolean isGreaterThanOrEqualTo(MonetaryAmount monetaryAmount) {
        return false;
    }

    @Override
    public boolean isLessThan(MonetaryAmount monetaryAmount) {
        return false;
    }

    @Override
    public boolean isLessThanOrEqualTo(MonetaryAmount monetaryAmount) {
        return false;
    }

    @Override
    public boolean isEqualTo(MonetaryAmount monetaryAmount) {
        return false;
    }

    @Override
    public int signum() {
        return 0;
    }

    @Override
    public MonetaryAmount add(MonetaryAmount monetaryAmount) {
        return null;
    }

    @Override
    public MonetaryAmount subtract(MonetaryAmount monetaryAmount) {
        return null;
    }

    @Override
    public MonetaryAmount multiply(long l) {
        return null;
    }

    @Override
    public MonetaryAmount multiply(double v) {
        return null;
    }

    @Override
    public MonetaryAmount multiply(Number number) {
        return null;
    }

    @Override
    public MonetaryAmount divide(long l) {
        return null;
    }

    @Override
    public MonetaryAmount divide(double v) {
        return null;
    }

    @Override
    public MonetaryAmount divide(Number number) {
        return null;
    }

    @Override
    public MonetaryAmount remainder(long l) {
        return null;
    }

    @Override
    public MonetaryAmount remainder(double v) {
        return null;
    }

    @Override
    public MonetaryAmount remainder(Number number) {
        return null;
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(long l) {
        return new MonetaryAmount[0];
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(double v) {
        return new MonetaryAmount[0];
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(Number number) {
        return new MonetaryAmount[0];
    }

    @Override
    public MonetaryAmount divideToIntegralValue(long l) {
        return null;
    }

    @Override
    public MonetaryAmount divideToIntegralValue(double v) {
        return null;
    }

    @Override
    public MonetaryAmount divideToIntegralValue(Number number) {
        return null;
    }

    @Override
    public MonetaryAmount scaleByPowerOfTen(int i) {
        return null;
    }

    @Override
    public MonetaryAmount abs() {
        return null;
    }

    @Override
    public MonetaryAmount negate() {
        return null;
    }

    @Override
    public MonetaryAmount plus() {
        return null;
    }

    @Override
    public MonetaryAmount stripTrailingZeros() {
        return null;
    }

    @Override
    public int compareTo(MonetaryAmount monetaryAmount) {
        return 0;
    }

    @Override
    public CurrencyUnit getCurrency() {
        return null;
    }

    @Override
    public NumberValue getNumber() {
        return null;
    }
}
