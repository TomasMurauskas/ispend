package com.example.ispend.money.classes;

import com.example.ispend.repository.BudgetRepository;
import com.example.ispend.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

@RequiredArgsConstructor
public class MonetaryAmmount {

    private final BudgetRepository budgetRepository;
    private final TransactionRepository transactionRepository;

    public MonetaryAmount getBudgetItemMoney (long id) {
        MonetaryAmount budgetItemMonetaryAmount = Monetary.getDefaultAmountFactory()
                .setCurrency(budgetRepository.getById(id).getCurrency())
                .setNumber(budgetRepository.getById(id).getAmount()).create();
        return budgetItemMonetaryAmount;
    }

    public MonetaryAmount getTransactionItemMoney (long id) {
        MonetaryAmount transactionItemMonetaryAmount = Monetary.getDefaultAmountFactory()
                .setCurrency(transactionRepository.getById(id).getCurrency())
                .setNumber(transactionRepository.getById(id).getAmount()).create();
        return transactionItemMonetaryAmount;
    }




}
