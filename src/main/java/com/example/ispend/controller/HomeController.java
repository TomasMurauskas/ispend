package com.example.ispend.controller;

import com.example.ispend.DateHandlers.DateRange;
import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.repository.BudgetRepository;
import com.example.ispend.repository.CategoryRepository;
import com.example.ispend.repository.TransactionRepository;
import com.example.ispend.request.BudgetItemRequest;
import com.example.ispend.request.CategoryItemRequest;
import com.example.ispend.request.TransactionItemRequest;
import com.example.ispend.service.BudgetCalculator;
import com.example.ispend.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class HomeController {

    private final BudgetRepository budgetRepository;
    private final CategoryRepository categoryRepository;
    private final TransactionRepository transactionRepository;
    private BudgetCalculator budgetCalculator;
    private final TransactionService transactionService;

    public HomeController(TransactionService transactionService, BudgetRepository budgetRepository, CategoryRepository categoryRepository, TransactionRepository transactionRepository, BudgetCalculator budgetCalculator) {
        this.budgetRepository = budgetRepository;
        this.categoryRepository = categoryRepository;
        this.transactionRepository = transactionRepository;
        this.budgetCalculator = budgetCalculator;
        this.transactionService = transactionService;
    }


    @RequestMapping("/")
    public ModelAndView home(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        ModelAndView model = new ModelAndView("home");
        model.addObject("budgetItems", budgetRepository.findAll());
        model.addObject("categories", categoryRepository.findAllByUserIdAndOrderByCategoryCategoryAsc(userPrincipal.getId()));
        model.addObject("transactions", transactionRepository.findByUserId(userPrincipal.getId()));
        return model;
    }


//TODO date filter
    @RequestMapping("/home/categories")
    public ModelAndView categories(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        ModelAndView model = new ModelAndView("categories");
        var categoriesRequest = categoryRepository.findAllByUserIdAndOrderByCategoryCategoryAsc(userPrincipal.getId())
                .stream()
                .map(categoryEntity -> {
                    var request = new CategoryItemRequest();
                    request.setCategory(categoryEntity.getCategory());
                    request.setDescription(categoryEntity.getDescription());
                    //TODO: The date is hardcoded,but it should be filtered by selection!!!
                    request.setBalance(budgetCalculator.totalBalanceOfCategory(userPrincipal.getId(), categoryEntity.getId(), LocalDate.of(2000, 01, 01), LocalDate.of(2022, 01, 01)).toString());
                    request.setUserName(categoryEntity.getUserEntity().getUserName());
                    return request;
                })
                .collect(Collectors.toList());

        model.addObject("categories", categoriesRequest);
        return model;
    }


    //TODO date filter
    @RequestMapping("/home/budgetItems")
    public ModelAndView budgetItems(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        ModelAndView model = new ModelAndView("budgetItems");
        var budgetItems = budgetRepository.findAllBudgetItemsByUserId(userPrincipal.getId());
        var budgetItemRequests = budgetItems
                .stream()
                .map(budgetItem -> {
                    var request = new BudgetItemRequest();
                    request.setTransactionItem(budgetItem.getBudgetItem());
                    request.setCurrency(budgetItem.getCurrency());
                    request.setAmount(budgetItem.getAmount());
                    request.setEndDate(budgetItem.getEndDate());
                    request.setStartDate(budgetItem.getStartDate());
                    request.setBalance(budgetCalculator.balanceOfBudgetItem(userPrincipal.getId(), budgetItem.getId(), budgetItem.getStartDate(), budgetItem.getEndDate()).toString());
                    return request;
                })
                .collect(Collectors.toList());

        model.addObject("budgetItems", budgetItemRequests);
        return model;
    }

    @RequestMapping("/home/transactions")
    public ModelAndView transactions(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        ModelAndView model = new ModelAndView("transactions");
        model.addObject("dateRange", new DateRange());
        model.addObject("transactions", transactionService.getAllTransactions(userPrincipal.getId()));
        return model;
    }

    @RequestMapping(value="/home/transactions/byDates", method = RequestMethod.POST)
    public String filterTransactions(@ModelAttribute(value = "dateRange") DateRange dateRange,
                                     Model model,
                                     @AuthenticationPrincipal UserPrincipal userPrincipal) {
    model.addAttribute("transactions", transactionService.filterTransactionsByDate(userPrincipal.getId(), dateRange));
    return "transactions";
    }

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

    @RequestMapping("/logout-success")
    public String logoutPage() {
        return "logout";
    }
}
