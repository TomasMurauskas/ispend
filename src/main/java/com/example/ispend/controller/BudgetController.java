package com.example.ispend.controller;


import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.BudgetItemEntity;
import com.example.ispend.repository.BudgetRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
//
//@Controller
//
//public class BudgetController {
//
//    private final BudgetRepository budgetRepository;
//
//    public BudgetController(BudgetRepository budgetRepository) {
//        this.budgetRepository = budgetRepository;
//    }
//
//    @GetMapping("/budgetItemView")
//    String budgetItems(Model model) {
//        model.addAttribute("budgetItems", budgetRepository.findAllBudgetItemsByUserId(1));
//        return "home.html";
//    }
//
//}
