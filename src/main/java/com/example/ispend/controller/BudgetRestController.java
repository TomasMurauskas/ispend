package com.example.ispend.controller;

import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.request.BudgetItemRequest;
import com.example.ispend.service.BudgetCalculator;
import com.example.ispend.service.BudgetService;
import lombok.RequiredArgsConstructor;
import org.javamoney.moneta.Money;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.money.NumberValue;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/budgetItems/")
public class BudgetRestController {

    private final BudgetService budgetService;
    private final BudgetCalculator budgetCalculator;

    //WORKS
    @GetMapping
    public List<BudgetItemRequest> getAllBudgetItemsByUserId(
            @AuthenticationPrincipal UserPrincipal userPrincipal
    ) {
        return budgetService.getAllBudgetItemsByUserId(userPrincipal.getId());
    }


    //WORKS
    @GetMapping("/budgetItemAmount/")
    public BigDecimal budgetItemAmount(@RequestParam long id) {
        return budgetCalculator.budgetItemAmount(id).getNumberStripped();
    }
    //WORKS
    @GetMapping("/budgetItemTransactionSum/")
    public BigDecimal budgetItemTransactionsSum (@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                  @RequestParam long budgetItemId,
                                                  @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                  @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return budgetCalculator.budgetItemTransactionsSum(userPrincipal.getId() , budgetItemId, startDate, endDate).getNumberStripped();
    }

    //WORKS
    @GetMapping("/balance/{id}")
    public double getBalanceOfBudgetItem(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                         @RequestParam long budgetItemId,
                                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return budgetCalculator.balanceOfBudgetItem(userPrincipal.getId(), budgetItemId, startDate, endDate).getNumber().doubleValueExact();
    }
    //WORKS
    @GetMapping("/totalUserBalance/")
    public BigDecimal getTotalBalance(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) throws IOException {
        return budgetCalculator.totalUserBudget(userPrincipal.getId(), startDate, endDate).getNumberStripped();
    }

    //WORKS
    @PostMapping
    public void addNewBudgetItem(
            @RequestBody @Validated BudgetItemRequest request,
            @AuthenticationPrincipal UserPrincipal userPrincipal )
    {
       budgetService.addNewBudgetItem(request, userPrincipal.getId());
    }

}
