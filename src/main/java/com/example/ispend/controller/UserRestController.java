package com.example.ispend.controller;


import com.example.ispend.entity.UserEntity;
import com.example.ispend.request.UserRequest;
import com.example.ispend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/users/")
public class UserRestController {

    private final UserService userService;

    //GET
    @GetMapping
    public List<UserEntity> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("{userId}")
    public UserEntity getUserById(@PathVariable long userId) {
        return userService.getUserById(userId).orElseThrow();
    }

    @GetMapping("{userName}")
    public UserEntity findByUserName(@PathVariable String userName) {
        return userService.findByUserName(userName);};

    //POST
    @PostMapping
    public void addNewUser(@RequestBody @Validated UserRequest request) {
        userService.save(request);
    }



    //DELETE
    @DeleteMapping("{userId}") //deleteUser
    public Optional<UserEntity> deleteUser(@PathVariable long userId) {
        Optional<UserEntity> userToDelete = userService.getUserById(userId);
        userService.deleteUserById(userToDelete.get().getUserId());
        return userToDelete;
    }

    //PUT/UPDATE
    @PutMapping(path="{userId}")
    public void updateOrSaveUser(@RequestBody UserRequest userRequest, @PathVariable long userId) {
        userService.UpdateUser(userRequest, userId);
    }
}
