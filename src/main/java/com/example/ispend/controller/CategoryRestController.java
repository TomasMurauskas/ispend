package com.example.ispend.controller;

import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.CategoryEntity;
import com.example.ispend.request.CategoryItemRequest;
import com.example.ispend.service.BudgetCalculator;
import com.example.ispend.service.CategoryService;
import org.javamoney.moneta.Money;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.money.NumberValue;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/categories/")
public class CategoryRestController {

    public CategoryRestController(CategoryService categoryService, BudgetCalculator budgetCalculator) {
        this.categoryService = categoryService;
        this.budgetCalculator = budgetCalculator;
    }

    private final CategoryService categoryService;
    private final BudgetCalculator budgetCalculator;

    @GetMapping
    public List<CategoryItemRequest> getAllCategories(
            @AuthenticationPrincipal UserPrincipal userPrincipal
            ) {
        return categoryService.getAllCategories(userPrincipal.getId());
    }

    @PostMapping
    public CategoryEntity addNewCategory(CategoryEntity categoryEntity) {
        return categoryService.addNewCategory(categoryEntity);
    }

    @GetMapping("/balance/")
    public Money totalBalanceOfCategory(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                        @RequestParam long categoryId,
                                        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  LocalDate endDate) {
        return  budgetCalculator.totalBalanceOfCategory(userPrincipal.getId(), categoryId, startDate, endDate);

    }

    @GetMapping("/balanceOfParentCategory/")
    public NumberValue totalBudgetOfParentCategory(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                   @RequestParam long parentCategoryId,
                                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  LocalDate endDate
    ) {
       return budgetCalculator.totalBalanceOfParentCategory(userPrincipal.getId(), parentCategoryId, startDate, endDate).getNumber();
    }
}
