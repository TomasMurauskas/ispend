package com.example.ispend.controller;


import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.TransactionEntity;
import com.example.ispend.request.TransactionItemRequest;
import com.example.ispend.service.TransactionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/transactions/")
public class TransactionRestController {

    TransactionService transactionService;

    public TransactionRestController(TransactionService transactionService) {
        this.transactionService=transactionService;
    }
//
    @GetMapping
    public List<TransactionItemRequest> getAllTransactions(
            @AuthenticationPrincipal UserPrincipal userPrincipal
            ) {
        return transactionService.getAllTransactions(userPrincipal.getId());
    }

    @GetMapping("/dates/")
    public List<TransactionItemRequest> findTransactionsByDateAndUserId (
            @AuthenticationPrincipal UserPrincipal userPrincipal,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate)
    {
        return transactionService.findTransactionsByDateAndUserId(userPrincipal.getId(), startDate, endDate);
    }

    @PostMapping(path="/transactions/", consumes = {"application/json"})
    public TransactionEntity addTransactionsEntity(@RequestBody TransactionEntity transactionEntity) {
       return transactionService.saveNewTransaction(transactionEntity);
    }

    @PutMapping(path="/transactions/{id}", consumes = {"application/json"})
    public void updateTransactionsEntity(@RequestBody TransactionEntity newTransactionEntity, @PathVariable  long id) {
        transactionService.saveOrUpdateTransaction(newTransactionEntity, id);
    }

    @DeleteMapping("/transactions/{id}")
    public void deleteTransaction (@PathVariable long id){
        transactionService.deleteTransaction(id);
    }
}
