package com.example.ispend.service;

import com.example.ispend.repository.BudgetRepository;
import com.example.ispend.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class BudgetCalculator {

    private final BudgetRepository budgetRepository;
    private final TransactionService transactionService;
    private final CategoryRepository categoryRepository;

    //WORKS
    public Money budgetItemAmount(long budgetItemId) {
        var item = budgetRepository.getById(budgetItemId);
      return Money.of(item.getAmount(), item.getCurrency());
    }
    //WORKS
    public Money budgetItemTransactionsSum(long userId, long budgetItemId, LocalDate startDate, LocalDate endDate) {

        var transactionItemRequests = transactionService.findTransactionsByDateAndUserIdAndBudgetItemId(userId, budgetItemId, startDate, endDate);

        return transactionItemRequests.stream()
                .map(request -> Money.of(request.getAmount(), request.getCurrency()))
                .reduce(Money.of(0, "EUR"), Money::add);
    }
    //WORKS
    public Money balanceOfBudgetItem(long userId, long budgetItemId, LocalDate startDate, LocalDate endDate) {
        var budgetItem = budgetRepository.getById(budgetItemId);
        var transactionItemRequests = (transactionService.findTransactionsByDateAndUserIdAndBudgetItemId(userId, budgetItemId, startDate, endDate));
        var budgetItemAmount = (Money.of(budgetItem.getAmount(), budgetItem.getCurrency()));

        return budgetItemAmount.subtract( transactionItemRequests.stream()
                                                                 .map(transactionItem -> Money.of(transactionItem.getAmount(), transactionItem.getCurrency()))
                                                                 .reduce(Money.of(0, "EUR"), Money::add));

    }

    public Money totalBalanceOfCategory(long userId, long categoryId, LocalDate startDate, LocalDate endDate) {

        var budgetItemList = budgetRepository.findBudgetItemByIdAndDate(userId, categoryId, startDate, endDate);

        return budgetItemList.stream().map(budgetItem -> balanceOfBudgetItem(userId, budgetItem.getId(), startDate, endDate))
                                      .reduce(Money.of(0, "EUR"), Money::add);
    }

    public Money totalBalanceOfParentCategory(long userId, long parentCategoryId, LocalDate startDate, LocalDate endDate) {
        var categoryList = categoryRepository.findAllByIdAndParentCategoryId(userId, parentCategoryId);
        //Get total Balance of Category

         return categoryList.stream().map(categoryEntity -> totalBalanceOfCategory(userId, categoryEntity.getId(), startDate, endDate))
                        .reduce(Money.of(0, "EUR"), Money::add);

    }

    public Money totalUserBudget(long userId, LocalDate startDate, LocalDate endDate)  {

        var userCategoryList = categoryRepository.findAllCategoriesByUserId(userId);
        return userCategoryList.stream().map(categoryEntity -> totalBalanceOfCategory(userId, categoryEntity.getId(), startDate, endDate))
                               .reduce(Money.of(0, "EUR"), Money::add);
    }
}
