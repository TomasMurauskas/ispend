package com.example.ispend.service;

import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.UserEntity;
import com.example.ispend.repository.UserRepository;
import com.example.ispend.request.UserRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private  final PasswordEncoder passwordEncoder;

    //POST
    public void save(UserRequest request) {
         var newUserEntity = new UserEntity();

         newUserEntity.setUserName(request.getUserName());
         newUserEntity.setEmail(request.getEmail());
         newUserEntity.setPassword(passwordEncoder.encode(request.getPassword()));
       userRepository.save(newUserEntity);
    }

    //GET
    public List<UserEntity> getAllUsers() {
         return userRepository.findAll();
     };
    public UserEntity findByUserName(String userName){
        return userRepository.findByUserName(userName);
    };
    public Optional<UserEntity> getUserById(long userId) {
        return userRepository.findById(userId);
    }

    //DELETE
    public void deleteUserById(long userId) {
        userRepository.deleteById(userId);
    }

    //PUT
    public void UpdateUser(UserRequest userRequest, long userId) {
        userRepository.findById(userId).map(
                userEntity -> {
                    userEntity.setUserName(userRequest.getUserName());
                    userEntity.setEmail(userRequest.getEmail());
                    userEntity.setPassword(passwordEncoder.encode(userRequest.getPassword()));
                    return userRepository.save(userEntity);
                }
        );
    }
}
