package com.example.ispend.service;

import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.BudgetItemEntity;
import com.example.ispend.repository.BudgetRepository;
import com.example.ispend.repository.CategoryRepository;
import com.example.ispend.repository.UserRepository;
import com.example.ispend.request.BudgetItemRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BudgetService {



    private final BudgetRepository budgetRepository;
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    private UserPrincipal userPrincipal;

    public BudgetService(BudgetRepository budgetRepository, CategoryRepository categoryRepository, UserRepository userRepository) {
        this.budgetRepository = budgetRepository;
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
    }


//GET

    public List<BudgetItemRequest> getAllBudgetItemsByUserId(long userId) {

        return budgetRepository.findAllBudgetItemsByUserId(userId).stream()
                .map(budgetEntity -> {
                    var request = new BudgetItemRequest();

                    request.setAmount(budgetEntity.getAmount());
                    request.setCategoryId(budgetEntity.getCategoryEntity().getId());
                    request.setCurrency(budgetEntity.getCurrency());
                    request.setTransactionItem(budgetEntity.getBudgetItem());
                    request.setStartDate(budgetEntity.getStartDate());
                    request.setEndDate(budgetEntity.getEndDate());
                    return request;
                })
                .collect(Collectors.toList());

    }

    //POST
    public void addNewBudgetItem(BudgetItemRequest request, long userId) {
        var entity = new BudgetItemEntity();
        entity.setCategoryEntity(categoryRepository.getById(request.getCategoryId()));
        entity.setBudgetItem(request.getTransactionItem());
        entity.setStartDate(request.getStartDate());
        entity.setEndDate(request.getEndDate());
        entity.setCurrency(request.getCurrency());
        entity.setAmount(request.getAmount());
        entity.setCreatedAt(Instant.now());
        entity.setUserEntity(userRepository.getById(userId));
        budgetRepository.save(entity);
    }

    //PUT
    public void updateBudgetItem(BudgetItemRequest budgetItemRequest, long id)  {
        budgetRepository.findById(id).map(
                budgetItemEntity -> {
                    budgetItemEntity.setUserEntity(userRepository.getById(userPrincipal.getId()));
                    budgetItemEntity.setCategoryEntity(categoryRepository.getById(budgetItemRequest.getCategoryId()));
                    budgetItemEntity.setBudgetItem(budgetItemRequest.getTransactionItem());
                    budgetItemEntity.setStartDate(budgetItemRequest.getStartDate());
                    budgetItemEntity.setEndDate(budgetItemRequest.getEndDate());
                    budgetItemEntity.setAmount(budgetItemRequest.getAmount());
                    budgetItemEntity.setCurrency(budgetItemRequest.getCurrency());
                    return budgetRepository.save(budgetItemEntity);
                }
        );
    }

    //DELETE
    public void deleteBudgetItemById(long id) {
        budgetRepository.deleteById(id);
    }




}

