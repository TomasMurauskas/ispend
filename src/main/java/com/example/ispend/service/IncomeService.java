package com.example.ispend.service;

import com.example.ispend.repository.IncomeRepository;
import com.example.ispend.request.IncomeItemRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IncomeService {

    private final IncomeRepository incomeRepository;

    public IncomeService( IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }
    public List<IncomeItemRequest> getIncomeByUserIdAndDate(Long id, LocalDate startDate, LocalDate endDate) {
        return incomeRepository.findAllByIdAndDate(id, startDate, endDate).stream()
                .map(incomeEntity -> {
                    var request = new IncomeItemRequest();
                    request.setIncome(incomeEntity.getIncome());
                    request.setCurrency(incomeEntity.getCurrency());
                    request.setCreatedByUserId(id);
                    request.setCreatedAt(incomeEntity.getCreatedAt());
                    return request;
                }
                ).collect(Collectors.toList());
    }
}
