package com.example.ispend.service;
import com.example.ispend.DateHandlers.DateRange;
import com.example.ispend.auth.UserPrincipal;
import com.example.ispend.entity.TransactionEntity;
import com.example.ispend.repository.TransactionRepository;
import com.example.ispend.request.TransactionItemRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionService {


    private final TransactionRepository transactionRepository;
    UserPrincipal userPrincipal;


    public List<TransactionItemRequest> getAllTransactions(Long userId) {
        return transactionRepository.findByUserId(userId).stream()
            .map(entity -> {
                var request = new TransactionItemRequest();
                    request.setTransactionItem(entity.getTransactionItem());
                    request.setCreatedByUserId(entity.getId());
                    request.setCategoryId(entity.getCategoryEntity().getId());
                    request.setAmount(entity.getAmount());
                    request.setCurrency(entity.getCurrency());
                    request.setTransactionDate(entity.getTransactionDate());
                    request.setDescription(entity.getDescription());
                    request.setCreatedAt(entity.getCreatedAt());
                    request.setBudgetItemId(entity.getBudgetItemEntity().getId());
                    return request;
                })
            .collect(Collectors.toList());
    }

    //POST
    public TransactionEntity saveNewTransaction(TransactionEntity transactionEntity) {
        return  transactionRepository.save(transactionEntity);
    };

    //DELETE
    public void deleteTransaction(long id) {
        transactionRepository.deleteById(id);
    };

    //PUT
    public void saveOrUpdateTransaction(TransactionEntity newTransactionEntity, long id) {
        transactionRepository.findById(id).map(
                transactionEntity -> {
                    transactionEntity.setTransactionDate(newTransactionEntity.getTransactionDate());
                    transactionEntity.setTransactionItem(newTransactionEntity.getTransactionItem());
                    transactionEntity.setAmount(newTransactionEntity.getAmount());
                    transactionEntity.setCurrency(newTransactionEntity.getCurrency());
                    transactionEntity.setDescription(newTransactionEntity.getDescription());
                    transactionEntity.setCreatedAt(newTransactionEntity.getCreatedAt());
                    return newTransactionEntity;
                }
        ).orElseGet(() -> {
            return transactionRepository.save(newTransactionEntity);
        });
    }

    public List<TransactionItemRequest> findTransactionsByDateAndUserId (Long userId, LocalDate startDate, LocalDate endDate) {

        return transactionRepository.findTransactionsByDateAndUserId (userId, startDate, endDate).stream().map(
                transactionEntity -> {
                    var request = new TransactionItemRequest();
                    request.setTransactionItem(transactionEntity.getTransactionItem());
                    request.setTransactionDate(transactionEntity.getTransactionDate());
                    request.setCurrency(transactionEntity.getCurrency());
                    request.setAmount(transactionEntity.getAmount());
                    request.setBudgetItemId(transactionEntity.getBudgetItemEntity().getId());
                    request.setDescription(transactionEntity.getDescription());
                    request.setCategoryId(transactionEntity.getId());
                    return request;
                }
        ).collect(Collectors.toList());
    }

    public List<TransactionEntity> findTransactionsByDateAndUserIdAndBudgetItemId(long userId, long budgetItemId, LocalDate startDate, LocalDate endDate) {
        return transactionRepository.findTransactionEntitiesByUserIdAndBudgetItemIdAndTransactionDateIsBetween (userId, budgetItemId, startDate, endDate);
    }

    public List<TransactionItemRequest> filterTransactionsByDate(Long id, DateRange dateRange) {
        return transactionRepository.findByUserId(id)
                .stream()
                .map(transactionEntity -> {
                    var request = new TransactionItemRequest();
                    request.setAmount(transactionEntity.getAmount());
                    request.setTransactionItem(transactionEntity.getTransactionItem());
                    request.setCurrency(transactionEntity.getCurrency());
                    request.setDescription(transactionEntity.getDescription());
                    request.setTransactionDate(transactionEntity.getTransactionDate());
                    request.setCategoryId(transactionEntity.getCategoryEntity().getId());
                    request.setCreatedByUserId(transactionEntity.getUserEntity().getUserId());
                    return request;})
                .filter(transactionItemRequest -> transactionItemRequest.getTransactionDate().isAfter(dateRange.getStartDate()))
                .filter(transactionItemRequest -> transactionItemRequest.getTransactionDate().isBefore(dateRange.getEndDate()))
                .collect(Collectors.toList());
    }
}
