package com.example.ispend.service;

import com.example.ispend.entity.CategoryEntity;
import com.example.ispend.repository.CategoryRepository;
import com.example.ispend.request.CategoryItemRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;


    public List<CategoryItemRequest> getAllCategories(Long userId) {
        return categoryRepository.findAllCategoriesByUserId(userId).stream()
                .map(entity  -> {
                    var request = new CategoryItemRequest();
                    request.setCategory(entity.getCategory());
                    request.setDescription(entity.getDescription());
                    request.setCreatedAt(entity.getCreatedAt());
                    request.setCreatedByUserId(entity.getId());
                    request.setParentCategoryId(entity.getParentCategoryId());
                    return request;
                }).collect(Collectors.toList());
    }


    public CategoryEntity addNewCategory(CategoryEntity categoryEntity) {
        return categoryRepository.save(categoryEntity);
    }
}
