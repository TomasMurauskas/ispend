package com.example.ispend.request;

import com.example.ispend.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
public class IncomeItemRequest {

    private long id;

    private Long createdByUserId;

    private BigDecimal income;

    private String currency;

    private Instant createdAt;
}
