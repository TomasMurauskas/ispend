package com.example.ispend.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
@Setter
public class CategoryItemRequest {

    @NotBlank
    private String category;

    @NotNull
    private Long createdByUserId;


    private long parentCategoryId;


    private String description;

    @NonNull
    private Instant createdAt;

    private String balance;

    private String userName;




}
