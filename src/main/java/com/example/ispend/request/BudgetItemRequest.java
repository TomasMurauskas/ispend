package com.example.ispend.request;

import lombok.Getter;
import lombok.Setter;
import org.javamoney.moneta.Money;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDate;


@Getter
@Setter
public class BudgetItemRequest {


    @NotBlank
    private String transactionItem;

    @NonNull
    private Long categoryId;

    @NonNull
    private BigDecimal amount;

    @NotBlank
    private String currency;

    @NonNull
    private LocalDate startDate;

    @NonNull
    private LocalDate endDate;

    @NonNull
    private String balance;
}
