package com.example.ispend.request;


import lombok.Getter;
import lombok.Setter;


import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

@Setter
@Getter
public class TransactionItemRequest {

 private String transactionItem;

 private Long createdByUserId;

 private Long categoryId;

 private BigDecimal amount;

 private String currency;

 private LocalDate transactionDate;

 private Instant createdAt;

 private String description;

 private Long BudgetItemId;

}
