package com.example.ispend.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UserRequest {

    @NotBlank
    private String userName;
    @NotBlank
    private String email;
    @NotBlank
    private String password;
}
