package com.example.ispend.repository;

import com.example.ispend.entity.BudgetItemEntity;
import com.example.ispend.entity.CategoryEntity;
import com.example.ispend.request.BudgetItemRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BudgetRepository extends JpaRepository<BudgetItemEntity, Long> {

    @Query("SELECT b FROM BudgetItemEntity AS b JOIN b.userEntity u WHERE u.userId = :userId")
    public List<BudgetItemEntity> findAllBudgetItemsByUserId(long userId);

    @Query ("SELECT b FROM BudgetItemEntity AS b WHERE b.userEntity.userId = :userId AND b.categoryEntity.id = :id AND (b.endDate <= :endDate AND b.startDate >= :startDate) ")
    List<BudgetItemEntity> findBudgetItemByIdAndDate(long userId, long id, LocalDate startDate, LocalDate endDate);

}
