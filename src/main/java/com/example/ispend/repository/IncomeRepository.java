package com.example.ispend.repository;

import com.example.ispend.entity.IncomeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface IncomeRepository extends JpaRepository<IncomeEntity, Long> {

    @Query("SELECT i FROM IncomeEntity AS i WHERE i.userEntity.userId = :userId AND i.createdAt >=:startDate AND i.createdAt <= :endDate")
    public List<IncomeEntity> findAllByIdAndDate (Long userId, LocalDate startDate, LocalDate endDate);
}
