package com.example.ispend.repository;


import com.example.ispend.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    @Query("Select c FROM CategoryEntity as c JOIN c.userEntity u WHERE u.userId = :userId ")
    public List<CategoryEntity> findAllCategoriesByUserId(long userId);

    @Query("SELECT c FROM CategoryEntity AS c WHERE c.userEntity.userId = :userId AND c.parentCategoryId = :id")
    public List<CategoryEntity> findAllByIdAndParentCategoryId(long userId, long id);

    @Query("SELECT c FROM CategoryEntity AS c  WHERE c.userEntity.userId = :userId ORDER BY c.category desc")
    public List<CategoryEntity> findAllByUserIdAndOrderByCategoryCategoryAsc (long userId);

}
