package com.example.ispend.repository;

import com.example.ispend.entity.TransactionEntity;
import com.example.ispend.request.TransactionItemRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import static org.hibernate.loader.Loader.SELECT;
@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {

    @Query("SELECT t FROM TransactionEntity t JOIN t.userEntity u WHERE u.userId = :userId")
    List<TransactionEntity> findByUserId(long userId);

    @Query("SELECT t FROM TransactionEntity t WHERE t.userEntity.userId = :userId AND t.transactionDate >= :startDate AND t.transactionDate <= :endDate ")
    List<TransactionEntity> findTransactionsByDateAndUserId (long userId, LocalDate startDate, LocalDate endDate);

    @Query("SELECT t FROM TransactionEntity t WHERE t.userEntity.userId = :userId AND t.budgetItemEntity.id = :budgetItemId AND t.transactionDate >= :startDate AND t.transactionDate <= :endDate ")
    List<TransactionEntity> findTransactionEntitiesByUserIdAndBudgetItemIdAndTransactionDateIsBetween (long userId, long budgetItemId,  LocalDate startDate, LocalDate endDate);
}
